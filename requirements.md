# Prérequis 

* Pour pouvoir jouer au jeu, vous avez besoin :
    * de Windows
    * d'un IDE comme [Thonny](https://thonny.org/). 
    * d'une résolution d'écran au miinimum de 1300 x 800 pixels, car le la fenêtre du jeu est de cette taille. Les écrans trop petits ne pourront pas voir la totalité de la fenêtre du jeu.
    * Du module Pygame.

* Installation du module Pygame avec l'exécuteur Thonny :
    * Cliquez sur "Outils" en haut à gauche, puis dans "Gérer les paquets...".
    * Entrez dans la barre de recherche "pygame", sélectionnez-le.
    * Cliquez sur "Installer" en bas de la fenêtre.

* Le jeu fonctionne avec plusieurs modules qui s'utilisent entre eux. Ces modules se trouvent dans le document "sources". Veuillez ne pas les toucher.

________

Auteurs AMEDRO Louis - LAPÔTRE Marylou - MAILLET Paul 

Licence CC BY SA